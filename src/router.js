import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const routes = [
    {
        path: "/",
        name: "home",
        component: Home
    },
    {
        path: "/JobCategory",
        name: "JobCategory",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import("./components/JobCategory.vue")
    },
    {
        path: "/JobForm",
        name: "JobForm",
        component: () => import("./components/JobForm.vue")
    },
    {
        path: "/SubCategory",
        name: "SubCategory",
        component: () => import("./components/SubCategory.vue")
    }
];
export default new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});
